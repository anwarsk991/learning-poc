##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install Packages needed for spark modules
#
#
####################################################################################################################
# install devtools to manage package version
install.packages("devtools",repos="https://cran.rstudio.com/")
library(devtools)

# packaged needed for all modules for spark running
needed <- c("stringi","sparklyr","testthat")
install.packages(needed,repos="http://cran.rstudio.com")

# install arrow for performance ehancement of sparklyr collect(), sdf_copy_to(), etc
devtools::install_github("apache/arrow", subdir = "r", ref = "apache-arrow-0.12.0")

# packages needed for sparkMessageSequence
needed <- c("futile.logger","RMySQL","Hmisc","openxlsx","RcppArmadillo","Rcpp","rgl","text2vec","uuid","properties","httr","RCurl","jsonlite","fpc","jiebaR","foreach","doMC","openssl","parallel")
inst <- installed.packages()
for(i in needed)
{
  if(!is.element(i,inst))
  {
    install.packages(i,repos="https://cran.rstudio.com/")
  }
}

# packages needed for sparkEngagement
needed <- c("RMySQL","ks","foreach","doMC","mvtnorm","rgl","uuid","properties","reshape2")
inst <- installed.packages()
for(i in needed)
{
  if(!is.element(i,inst))
  {
    install.packages(i,repos="https://cran.rstudio.com/")
  }
}

# additional packages needed for Anchor
needed <- c("reshape2","markovchain","Rsolnp","lattice","bizdays","geosphere","testthat")
inst <- installed.packages()
for(i in needed)
{
  if(!is.element(i,inst))
  {
    install.packages(i,repos="https://cran.rstudio.com/")
  }
}

# install specific version of data.table, h2o， reticulate
install_version("data.table", version = "1.11.4", repos = "https://cran.rstudio.com")
install_version("dplyr", version = "0.8.0", repos = "https://cran.rstudio.com")
install_version("sparklyr", version = "1.0.0", repos = "https://cran.rstudio.com")
install.packages("h2o", type="source", repos="http://h2o-release.s3.amazonaws.com/h2o/rel-yates/1/R") # 3.24.0.1
install_version("reticulate", version = "1.10", repos = "http://cran.rstudio.com")

# # install spark
# library(sparklyr)
# spark_install(version = "2.3.1")

# #
# # This code builds the learning package and then installs it
# #
# 
# # R CMD build learningPackage && R CMD INSTALL Learning_1.0.tar.gz
# shellCode <- sprintf("tar -cvf Learning.tar.gz %s/learningPackage",homedir)
# system(shellCode)
# # if(is.element("Learning",inst))remove.packages("Learning")
# install.packages("Learning.tar.gz", repos = NULL, type = "source")
# 
# # R CMD build sparkLearningPackage && R CMD INSTALL sparkLearning_1.0.tar.gz 
# shellCode <- sprintf("tar -cvf sparkLearning.tar.gz %s/sparkLearningPackage",homedir)
# system(shellCode)
# # if(is.element("sparkLearning",inst))remove.packages("sparkLearning")
# install.packages("sparkLearning.tar.gz", repos = NULL, type = "source")

